export interface QuestionNode {
  id: number;
  title: string;
  cssClass: string ;
  childs: QuestionNode[];
}
