export interface Question {
  id: string;
  value: string;
  imageUrl: string;
  yes: Question;
  no: Question;
}
