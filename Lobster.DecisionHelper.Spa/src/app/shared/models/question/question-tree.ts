import { QuestionNode } from './question-node';
export interface QuestionTree {
  questionnaireId: number;
  questionnaireTitle: string;
  root: QuestionNode;
}
