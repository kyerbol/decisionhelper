import { Questionnaire } from './questionnaire';

export interface QuestionnaireList {
  questionnaires: Questionnaire[];
  }
