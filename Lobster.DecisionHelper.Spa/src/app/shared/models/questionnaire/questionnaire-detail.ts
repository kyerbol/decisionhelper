import { Question } from './../question/question';
export interface QuestionnaireDetail {
  id: number;
  title: string;
  rootQuestion: Question;
}
