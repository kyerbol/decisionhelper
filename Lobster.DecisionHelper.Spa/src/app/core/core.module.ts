import { QuestionnaireService } from './services/questionnaire.service';
import { CoreRoutingModule } from './core-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NgModule, } from '@angular/core';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    NavbarComponent,
    MainLayoutComponent
  ],
  imports: [
    SharedModule,
    CoreRoutingModule
  ],
  providers: [
    QuestionnaireService
  ]
})
export class CoreModule { }
