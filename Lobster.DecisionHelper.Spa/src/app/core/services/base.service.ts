import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected url = environment.api_endpoint;

  constructor(protected http: HttpClient) { }
}
