import { QuestionnaireDetail } from './../../shared/models/questionnaire/questionnaire-detail';
import { QuestionnaireList } from './../../shared/models/questionnaire/questionnaire-list';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.url += 'questionnaires';
  }

  getAll(): Observable<QuestionnaireList> {
    return this.http.get<QuestionnaireList>(this.url);
  }

  getById(id: number): Observable<QuestionnaireDetail> {
    return this.http.get<QuestionnaireDetail>(this.url + '/' + id );
  }
}
