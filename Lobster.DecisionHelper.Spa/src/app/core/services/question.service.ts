import { QuestionTree } from './../../shared/models/question/question-tree';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.url += 'questions';
  }

  getQuestionTreeById(id: number): Observable<QuestionTree> {
    return this.http.get<QuestionTree>(this.url + '/' + id );
  }
}
