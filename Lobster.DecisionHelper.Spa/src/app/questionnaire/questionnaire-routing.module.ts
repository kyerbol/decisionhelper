import { QuestionTreeComponent } from './components/question-tree/question-tree.component';
import { QuestionnaireStartComponent } from './components/questionnaire-start/questionnaire-start.component';
import { QuestionnairesComponent } from './components/questionnaires/questionnaires.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: QuestionnairesComponent
  },
  {
    path: ':id', component: QuestionnaireStartComponent
  },
  {
    path: 'question/:id', component: QuestionTreeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class QuestionnaireRoutingModule { }
