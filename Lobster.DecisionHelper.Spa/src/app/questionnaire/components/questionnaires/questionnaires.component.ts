import { QuestionnaireList } from './../../../shared/models/questionnaire/questionnaire-list';
import { QuestionnaireService } from './../../../core/services/questionnaire.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-questionnaires',
  templateUrl: './questionnaires.component.html',
  styleUrls: ['./questionnaires.component.scss']
})
export class QuestionnairesComponent implements OnInit {
  questionnaireList: QuestionnaireList;
  count = 1;

  constructor(private questionnaireService: QuestionnaireService,
              private router: Router) { }

  ngOnInit(): void {
    this.questionnaireService.getAll().subscribe(
      (data: QuestionnaireList) => {
        this.questionnaireList = data;
      });
  }

  start(id: number): void{
    this.router.navigate(['/questionnaires/' + id]);
  }

}
