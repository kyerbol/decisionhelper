import { QuestionnaireService } from './../../../core/services/questionnaire.service';
import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionnaireDetail } from 'src/app/shared/models/questionnaire/questionnaire-detail';
import { Question } from 'src/app/shared/models/question/question';
import { BehaviorSubject, Observable } from 'rxjs';
import { faChevronRight, faSmileBeam, faFutbol } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-questionnaire-start',
  templateUrl: './questionnaire-start.component.html',
  styleUrls: ['./questionnaire-start.component.scss']
})
export class QuestionnaireStartComponent implements OnInit {
  private questionSubject = new BehaviorSubject<Question>(null);
  private id: number;
  question: Question;
  isLastQuestion: boolean;
  faChevronRight = faChevronRight;
  faSmileBeam = faSmileBeam;
  faFutbol = faFutbol;
  counter = 0;
  selected = false;
  questionnaireName: string;

  constructor(private route: ActivatedRoute,
              private questionnaireService: QuestionnaireService) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.questionnaireService.getById(this.id)
        .subscribe(
      (data: QuestionnaireDetail) => {
        this.questionnaireName = data.title;
        this.questionSubject.next(data.rootQuestion);
    });

    this.question$.subscribe(data =>
    {
      if (data){
        this.question = data;
        this.isLastQuestion = !this.question?.yes || !this.question?.no;
        this.counter++;
      }
    });
  }

  get question$(): Observable<Question> {
    return this.questionSubject.asObservable();
  }

  answer(isYes: boolean): void{
    this.questionSubject.next(isYes ? this.question.yes : this.question.no);
    this.selected = false;
  }
}
