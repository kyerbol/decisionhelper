import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTreeComponent } from './question-tree.component';

describe('QuestionTreeComponent', () => {
  let component: QuestionTreeComponent;
  let fixture: ComponentFixture<QuestionTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
