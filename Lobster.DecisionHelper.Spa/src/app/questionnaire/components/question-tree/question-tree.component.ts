import { QuestionNode } from './../../../shared/models/question/question-node';
import { QuestionTree } from './../../../shared/models/question/question-tree';
import { QuestionService } from './../../../core/services/question.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { faFutbol, faUndoAlt, faSmileBeam  } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-question-tree',
  templateUrl: './question-tree.component.html',
  styleUrls: ['./question-tree.component.scss']
})
export class QuestionTreeComponent implements OnInit {
  private id: number;
  nodes: QuestionNode[] = [];
  questionnaireTitle: string;
  questionnaireId: number;
  faFutbol = faFutbol;
  faUndoAlt = faUndoAlt;
  faSmileBeam = faSmileBeam;

  constructor(private route: ActivatedRoute,
              private questionService: QuestionService) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.questionService.getQuestionTreeById(this.id)
        .subscribe(
      (data: QuestionTree) => {
        this.questionnaireId = data.questionnaireId;
        this.questionnaireTitle = data.questionnaireTitle;
        this.nodes.push(data.root);
    });
  }

}
