import { QuestionnairesComponent } from './components/questionnaires/questionnaires.component';
import { NgModule } from '@angular/core';
import { QuestionnaireRoutingModule } from './questionnaire-routing.module';
import { SharedModule } from '../shared/shared.module';
import { QuestionnaireStartComponent } from './components/questionnaire-start/questionnaire-start.component';
import { NgxOrgChartModule } from 'ngx-org-chart';
import { QuestionTreeComponent } from './components/question-tree/question-tree.component';

@NgModule({
  imports: [
    SharedModule,
    QuestionnaireRoutingModule,
    NgxOrgChartModule
  ],
  declarations: [
    QuestionnairesComponent,
    QuestionnaireStartComponent,
    QuestionTreeComponent
  ]
})
export class QuestionnaireModule { }
