﻿using FluentAssertions;
using Lobster.DecisionHelper.Application.Common.Exceptions;
using Lobster.DecisionHelper.Application.Questions.Queries.GetQuestionTreeWithSelection;
using Lobster.DecisionHelper.Application.Tests.Shared;
using Lobster.DecisionHelper.Domain.Entities;
using Lobster.DecisionHelper.Infrastructure.Data;
using Microsoft.Extensions.Configuration;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using static Lobster.DecisionHelper.Application.Questions.Queries.GetQuestionTreeWithSelection.GetQuestionTreeWithSelectionQuery;

namespace Lobster.DecisionHelper.Application.Tests.Questions.Queries.GetQuestionTreeWithSelection
{
    public class GetQuestionTreeWithSelectionQueryTests
    {
        private readonly Mock<IDecisionHelperDbContext> _mockDbContext;
        private readonly Mock<IConfiguration> _configuration;

        public GetQuestionTreeWithSelectionQueryTests()
        {
            _mockDbContext = new Mock<IDecisionHelperDbContext>();
            _mockDbContext.Setup(p => p.Questions).Returns(TestData.Questions);

            _configuration = new Mock<IConfiguration>();
            _configuration.SetupGet(x => x[It.Is<string>(s => s == "AppSettings:NgxOrgChart:NgxQuestionSelectedCssClassName")]).Returns("ngx-question-selected");
        }

        [Fact]
        public void Should_ThrowFormatException_When_InvalidPathIsParsed()
        {
            var questions = new List<Question>
            {
                new Question
                {
                    Id = 1,
                    Text = "Are you sure you want to play football?",
                    IsPositive = true,
                    Path = "1",
                    QuestionnaireId = 1
                },
                new Question
                {
                    Id = 2,
                    Text = "Do you feel yourself healthy?",
                    IsPositive = true,
                    Path = @"1\@2",
                    ParentId = 1,
                    QuestionnaireId = 1
                },
                new Question
                {
                    Id = 3,
                    Text = "Right decision. It is summer outside. Go to the beach.",
                    IsPositive = false,
                    Path = @"1\@4",
                    ParentId = 1,
                    QuestionnaireId = 1,
                    ImageUrl = "https://picsum.photos/id/100/200"
                }
            };

            _mockDbContext.Setup(p => p.Questions).Returns(questions.AsQueryable().BuildMockDbSet().Object);
            GetQuestionTreeWithSelectionQuery query = new GetQuestionTreeWithSelectionQuery() { SelectedQuestionId = 3 };
            GetQuestionTreeWithSelectionQueryHandler handler = new GetQuestionTreeWithSelectionQueryHandler(_mockDbContext.Object, _configuration.Object);

            Func<Task> act = async () => await handler.Handle(query, new CancellationToken());

            act.Should().Throw<FormatException>();

        }


        [Theory]
        [InlineData(6)]
        [InlineData(25)]
        public void Should_ThrowNotFoundException_When_QuestionIdIsNotValid(int id)
        {
            GetQuestionTreeWithSelectionQuery query = new GetQuestionTreeWithSelectionQuery() { SelectedQuestionId = id };
            GetQuestionTreeWithSelectionQueryHandler handler = new GetQuestionTreeWithSelectionQueryHandler(_mockDbContext.Object, _configuration.Object);

            Func<Task> act = async () => await handler.Handle(query, new CancellationToken());

            act.Should().Throw<NotFoundException>();
        }


        [Fact]
        public void Shoud_ThrowFormatException_When_OneOfQuestionsPathIsInvalid()
        {
            GetQuestionTreeWithSelectionQuery query = new GetQuestionTreeWithSelectionQuery() { SelectedQuestionId = 1 };

            GetQuestionTreeWithSelectionQueryHandler handler = new GetQuestionTreeWithSelectionQueryHandler(_mockDbContext.Object, _configuration.Object);

            var questionnaires = new List<Questionnaire>
            {
                new Questionnaire
                {
                    Id = 1,
                    Title = "Can I play football today?",
                    CreatedDateTime = new DateTime(2020, 08, 24),
                    Questions = new List<Question>
                    {
                        new Question
                        {
                            Id = 1,
                            Text = "Are you sure you want to play football?",
                            IsPositive = true,
                            Path = "1",
                            QuestionnaireId = 1
                        },
                        new Question
                        {
                            Id = 2,
                            Text = "Do you feel yourself healthy?",
                            IsPositive = true,
                            Path = @"1\2",
                            ParentId = 1,
                            QuestionnaireId = 1
                        },
                        new Question
                        {
                            Id = 3,
                            Text = "Right decision. It is summer outside. Go to the beach.",
                            IsPositive = false,
                            Path = @"1\@4",
                            ParentId = 1,
                            QuestionnaireId = 1,
                            ImageUrl = "https://picsum.photos/id/100/200"
                        }
                    }
                }
            };

            _mockDbContext.Setup(p => p.Questionnaires).Returns(questionnaires.AsQueryable().BuildMockDbSet().Object);

            Func<Task> act = async () => await handler.Handle(query, new CancellationToken());

            act.Should().Throw<FormatException>();
        }


        [Fact]
        public async Task Shoud_SuccessfullyReturnQuestionTreeWithSelectionDto_When_AllDataAreValid()
        {
            GetQuestionTreeWithSelectionQuery query = new GetQuestionTreeWithSelectionQuery() { SelectedQuestionId = 1 };

            GetQuestionTreeWithSelectionQueryHandler handler = new GetQuestionTreeWithSelectionQueryHandler(_mockDbContext.Object, _configuration.Object);

            var questionnaires = new List<Questionnaire>
            {
                new Questionnaire
                {
                    Id = 1,
                    Title = "Can I play football today?",
                    CreatedDateTime = new DateTime(2020, 08, 24),
                    Questions = new List<Question>
                    {
                        new Question
                        {
                            Id = 1,
                            Text = "Are you sure you want to play football?",
                            IsPositive = true,
                            Path = "1",
                            QuestionnaireId = 1
                        },
                        new Question
                        {
                            Id = 2,
                            Text = "Do you feel yourself healthy?",
                            IsPositive = true,
                            Path = @"1\2",
                            ParentId = 1,
                            QuestionnaireId = 1
                        },
                        new Question
                        {
                            Id = 3,
                            Text = "Right decision. It is summer outside. Go to the beach.",
                            IsPositive = false,
                            Path = @"1\4",
                            ParentId = 1,
                            QuestionnaireId = 1,
                            ImageUrl = "https://picsum.photos/id/100/200"
                        },
                        new Question
                        {
                            Id = 4,
                            Text = "Stay at home until you are fully recovered.",
                            IsPositive = false,
                            Path = @"1\2\4",
                            ParentId = 2,
                            QuestionnaireId = 1,
                            ImageUrl = "https://picsum.photos/id/225/200"
                        },
                        new Question
                        {
                            Id = 5,
                            Text = "Is it a good weather outside?",
                            IsPositive = true,
                            Path = @"1\2\5",
                            QuestionnaireId = 1,
                            ParentId = 2
                        },
                    }
                }
            };
            _mockDbContext.Setup(p => p.Questionnaires).Returns(questionnaires.AsQueryable().BuildMockDbSet().Object);

            var result = await handler.Handle(query, new CancellationToken());

            result.Should().NotBeNull();
            result.QuestionnaireId.Should().Be(1);
            result.QuestionnaireTitle.Should().Be("Can I play football today?");
            result.Root.Id.Should().Be(1);
            result.Root.Childs.Count.Should().Be(2);
        }

    }
}
