﻿using FluentAssertions;
using Lobster.DecisionHelper.Application.Common.Exceptions;
using Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaire;
using Lobster.DecisionHelper.Application.Tests.Shared;
using Lobster.DecisionHelper.Domain.Entities;
using Lobster.DecisionHelper.Infrastructure.Data;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using static Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaire.GetQuestionnaireQuery;

namespace Lobster.DecisionHelper.Application.Tests.Questionnaires.Queries.GetQuestionnaire
{
    public class GetQuestionnaireQueryTests
    {
        private readonly Mock<IDecisionHelperDbContext> _mockDbContext;

        public GetQuestionnaireQueryTests()
        {
            _mockDbContext = new Mock<IDecisionHelperDbContext>();
            _mockDbContext.Setup(p => p.Questionnaires).Returns(TestData.Questionnaires);
        }

        [Theory]
        [InlineData(1, "Can I play football today?")]
        [InlineData(2, "Can I eat pizza today?")]
        public async Task Should_ReturnQuestionnaire_When_ValidQuestionnaireIdIsPassed(int id, string title)
        {
            GetQuestionnaireQuery query = new GetQuestionnaireQuery() { Id = id };
            GetQuestionnaireQueryHandler handler = new GetQuestionnaireQueryHandler(_mockDbContext.Object);

            var questionnaireDetailDto = await handler.Handle(query, new CancellationToken());

            questionnaireDetailDto.Should().NotBeNull();
            questionnaireDetailDto.Title.Should().Be(title);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(25)]
        public void Should_ThrowNotFoundException_When_QuestionnaireIdIsNotValid(int id)
        {
            GetQuestionnaireQuery query = new GetQuestionnaireQuery() { Id = id };
            GetQuestionnaireQueryHandler handler = new GetQuestionnaireQueryHandler(_mockDbContext.Object);

            Func<Task> act = async () => await handler.Handle(query, new CancellationToken());

            act.Should().Throw<NotFoundException>();
        }

        [Fact]
        public void Shoud_ThrowFormatException_When_InvalidPathIsParsed()
        {
            GetQuestionnaireQuery query = new GetQuestionnaireQuery() { Id = 1 };

            GetQuestionnaireQueryHandler handler = new GetQuestionnaireQueryHandler(_mockDbContext.Object);

            var questionnaires = new List<Questionnaire>
            {
                new Questionnaire
                {
                    Id = 1,
                    Title = "Can I play football today?",
                    CreatedDateTime = new DateTime(2020, 08, 24),
                    Questions = new List<Question>
                    {
                        new Question
                        {
                            Id = 1,
                            Text = "Are you sure you want to play football?",
                            IsPositive = true,
                            Path = "1",
                            QuestionnaireId = 1
                        },
                        new Question
                        {
                            Id = 2,
                            Text = "Do you feel yourself healthy?",
                            IsPositive = true,
                            Path = @"1\2",
                            ParentId = 1,
                            QuestionnaireId = 1
                        },
                        new Question
                        {
                            Id = 3,
                            Text = "Right decision. It is summer outside. Go to the beach.",
                            IsPositive = false,
                            Path = @"1\@4",
                            ParentId = 1,
                            QuestionnaireId = 1,
                            ImageUrl = "https://picsum.photos/id/100/200"
                        }
                    }
                }
            };

            _mockDbContext.Setup(p => p.Questionnaires).Returns(questionnaires.AsQueryable().BuildMockDbSet().Object);

            Func<Task> act = async () => await handler.Handle(query, new CancellationToken());

            act.Should().Throw<FormatException>();
        }
    }
}
