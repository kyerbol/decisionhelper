﻿using AutoMapper;
using FluentAssertions;
using Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList;
using Lobster.DecisionHelper.Application.Tests.Shared;
using Lobster.DecisionHelper.Infrastructure.Data;
using Moq;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using static Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList.GetQuestionnaireListQuery;

namespace Lobster.DecisionHelper.Application.Tests.Questionnaires.Queries.GetQuestionnaireList
{
    public class GetQuestionnaireListQueryTests : IClassFixture<BaseTestFixture>
    {
        private readonly Mock<IDecisionHelperDbContext> _mockDbContext;
        private readonly IMapper _mapper;

        public GetQuestionnaireListQueryTests(BaseTestFixture fixture)
        {
            _mockDbContext = new Mock<IDecisionHelperDbContext>();
            _mockDbContext.Setup(p => p.Questionnaires).Returns(TestData.Questionnaires);

            _mapper = fixture.CustomMapper;
        }


        [Fact]
        public async Task Should_ReturnQuestionnaireList_When_GetQuestionnaireListQueryIsCalled()
        {
            GetQuestionnaireListQuery query = new GetQuestionnaireListQuery();
            GetQuestionnaireListQueryHandler handler = new GetQuestionnaireListQueryHandler(_mockDbContext.Object, _mapper);

            var questionnaireListDto = await handler.Handle(query, new CancellationToken());

            questionnaireListDto.Questionnaires.Count().Should().Be(_mockDbContext.Object.Questionnaires.Count());
        }
    }
}
