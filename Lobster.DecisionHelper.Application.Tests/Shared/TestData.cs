﻿using Lobster.DecisionHelper.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lobster.DecisionHelper.Application.Tests.Shared
{
    public static class TestData
    {
        public static DbSet<Questionnaire> Questionnaires()
        {
            var questionnaires = new List<Questionnaire>
            {
                new Questionnaire
                {
                    Id = 1,
                    Title = "Can I play football today?",
                    CreatedDateTime = new DateTime(2020, 08, 24)
                },
                new Questionnaire
                {
                    Id = 2,
                    Title = "Can I eat pizza today?",
                    CreatedDateTime = new DateTime(2020, 08, 24)
                }
            };
            return questionnaires.AsQueryable().BuildMockDbSet().Object;
        }

        public static DbSet<Question> Questions()
        {
            var questions = new List<Question>
            {
                new Question
                {
                  Id = 1,
                  Text = "Are you sure you want to play football?",
                  IsPositive = true,
                  Path = "1",
                  QuestionnaireId = 1
                },
                new Question
                {
                    Id = 2,
                    Text = "Do you feel yourself healthy?",
                    IsPositive = true,
                    Path = @"1\2",
                    ParentId = 1,
                    QuestionnaireId = 1
                },
                new Question
                {
                    Id = 3,
                    Text = "Right decision. It is summer outside. Go to the beach.",
                    IsPositive = false,
                    Path = @"1\3",
                    ParentId = 1,
                    QuestionnaireId = 1,
                    ImageUrl = "https://picsum.photos/id/100/200"
                },
                new Question
                {
                    Id = 4,
                    Text = "Stay at home until you are fully recovered.",
                    IsPositive = false,
                    Path = @"1\2\4",
                    ParentId = 2,
                    QuestionnaireId = 1,
                    ImageUrl = "https://picsum.photos/id/225/200"
                },
                new Question
                {
                    Id = 5,
                    Text = "Is it a good weather outside?",
                    IsPositive = true,
                    Path = @"1\2\5",
                    QuestionnaireId = 1,
                    ParentId = 2
                },
            };
            return questions.AsQueryable().BuildMockDbSet().Object;
        }
    }
}
