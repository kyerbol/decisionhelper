﻿using AutoMapper;
using Lobster.DecisionHelper.Application.Common.Mappings;
using System;

namespace Lobster.DecisionHelper.Application.Tests.Shared
{
    public class BaseTestFixture : IDisposable
    {
        public IMapper CustomMapper { get; }

        public BaseTestFixture()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            CustomMapper = config.CreateMapper();
        }
        public void Dispose()
        {

        }
    }
}
