﻿using AutoMapper;
using FluentAssertions;
using Lobster.DecisionHelper.Application.Common.Mappings;
using Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList;
using Lobster.DecisionHelper.Domain.Entities;
using System;
using Xunit;

namespace Lobster.DecisionHelper.Application.Tests.Common.Mappings
{
    public class MappingProfileTests
    {
        private readonly IConfigurationProvider _configuration;
        private readonly IMapper _mapper;

        public MappingProfileTests()
        {
            _configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            _mapper = _configuration.CreateMapper();
        }

        [Fact]
        public void Should_HaveValidConfiguration_When_RunValidation()
        {
            _configuration.AssertConfigurationIsValid();
        }

        [Theory]
        [InlineData(typeof(Questionnaire), typeof(QuestionnaireDto))]
        public void Should_SupportMapping_When_MappingFromSourceToDestination(Type source, Type destination)
        {
            var instance = Activator.CreateInstance(source);

            var destInstance = _mapper.Map(instance, source, destination);

            destInstance.Should().BeOfType(destination);
            destInstance.Should().NotBeNull();
        }
    }
}
