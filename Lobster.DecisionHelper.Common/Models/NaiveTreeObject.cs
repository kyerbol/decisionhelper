﻿using Lobster.DecisionHelper.Common.Interfaces;

namespace Lobster.DecisionHelper.Common.Models
{
    public class NaiveTreeObject : INaiveTreeObject
    {
        public int Id { get; }
        public string Value { get; }
        public int[] Path { get; }
        public bool IsPositive { get; }
        public string ImageUrl { get; }

        public NaiveTreeObject(int id, string value, int[] path, bool isPostivie, string imageUrl)
        {
            Id = id;
            Value = value;
            Path = path;
            IsPositive = isPostivie;
            ImageUrl = imageUrl;
        }
    }
}
