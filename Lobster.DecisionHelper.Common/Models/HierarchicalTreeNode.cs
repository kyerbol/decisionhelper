﻿using System.Collections.Generic;

namespace Lobster.DecisionHelper.Common.Models
{
    /// <summary>
    /// Node of hierarchical tree.
    /// </summary>
    public class HierarchicalTreeNode
    {
        /// <summary>
        /// Id of node.
        /// </summary>
        public int Id { get; }

        /// <summary>
        ///  Title of node.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Css class of node.
        /// </summary>
        public string CssClass { get; }

        /// <summary>
        /// Name of node.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Childs of node.
        /// </summary>
        public IList<HierarchicalTreeNode> Childs { get; }

        public HierarchicalTreeNode(int id, string title, string cssClass, string name)
        {
            Id = id;
            Title = title;
            Childs = new List<HierarchicalTreeNode>();
            CssClass = cssClass;
            Name = name;
        }
    }
}