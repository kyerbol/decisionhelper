﻿using Lobster.DecisionHelper.Common.Interfaces;

namespace Lobster.DecisionHelper.Common.Models
{
    public class HierarchicalTreeObject : IHierarchicalTreeObject
    {
        public int Id { get; }

        public string Title { get; }

        public int? ParentId { get; }

        public int[] Path { get; }

        public string CssClass {get;}

        public bool IsPositive { get;  }

        public HierarchicalTreeObject(int id, string title, int? parentId, int[] path, string cssClass, bool isPositive)
        { 
            Id = id;
            Title = title;
            Path = path;
            ParentId = parentId;
            CssClass = cssClass;
            IsPositive = isPositive;
        }
    }
}
