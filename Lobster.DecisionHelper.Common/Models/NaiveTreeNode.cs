﻿namespace Lobster.DecisionHelper.Common.DataStructures.Models
{
    /// <summary>
    /// Node of naive tree.
    /// </summary>
    public class NaiveTreeNode
    {
        /// <summary>
        /// Id of the node.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Value of the node.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Image url of the node.
        /// </summary>
        public string ImageUrl { get; private set; }

        /// <summary>
        /// Yes child node of the node
        /// </summary>
        public NaiveTreeNode Yes { get; set; }

        /// <summary>
        /// No child node of the node.
        /// </summary>
        public NaiveTreeNode No { get; set; }

        public NaiveTreeNode(int id, string value, string imageUrl)
        {
            Id = id;
            Value = value;
            ImageUrl = imageUrl;
        }
    }
}
