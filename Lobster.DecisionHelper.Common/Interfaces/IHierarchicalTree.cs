﻿using Lobster.DecisionHelper.Common.Models;
using System.Collections.Generic;

namespace Lobster.DecisionHelper.Common.Interfaces
{
    public interface IHierarchicalTree
    {
        int Id { get; }
        string Title { get; }

        IEnumerable<HierarchicalTreeNode> Childs { get; }

        void Insert(IHierarchicalTreeObject input);
    }
}
