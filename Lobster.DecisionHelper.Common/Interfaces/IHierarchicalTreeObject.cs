﻿namespace Lobster.DecisionHelper.Common.Interfaces
{
    public interface IHierarchicalTreeObject
    {
        int Id { get; }

        string Title { get; }

        int? ParentId { get; }

        int[] Path { get; }

        bool IsPositive { get; }

        string CssClass{ get; }
    }
}
