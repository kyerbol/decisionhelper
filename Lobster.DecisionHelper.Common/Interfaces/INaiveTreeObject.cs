﻿namespace Lobster.DecisionHelper.Common.Interfaces
{
    public interface INaiveTreeObject
    {
        int Id { get; }

        string Value { get; }

        int[] Path { get; }

        bool IsPositive { get;  }

        string ImageUrl { get; }
    }
}
