﻿using Lobster.DecisionHelper.Common.DataStructures.Models;

namespace Lobster.DecisionHelper.Common.Interfaces
{
    public interface INaiveTree
    {
        NaiveTreeNode Root { get; set; }

        void Insert(INaiveTreeObject obj);
    }
}
