﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lobster.DecisionHelper.Common.Extensions
{
    public static class AppDomainExtensions
    {
        public static Assembly GetAssemblyByName(this AppDomain appDomain, string assemblyName)
        {
            return appDomain.GetAssemblies().FirstOrDefault(a => a.GetName().Name == assemblyName);
        }

        public static IEnumerable<Assembly> GetAssembliesStartWith(this AppDomain appDomain, string startWith)
        {
            return appDomain.GetAssemblies().Where(a => a.GetName().Name.StartsWith(startWith));
        }
    }
}
