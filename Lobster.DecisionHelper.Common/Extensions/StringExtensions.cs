﻿using System.Linq;

namespace Lobster.DecisionHelper.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool TrySplitByCharAndConverToIntArray(this string value, char splitChar, out int[] result)
        {
            result = new int[] { };

            if (value == null)
            {
                return false;
            }

            try
            {
                result = value.Split(splitChar).Select(int.Parse).ToArray();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
