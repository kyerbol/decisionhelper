﻿namespace Lobster.DecisionHelper.Common.Extensions
{
    public static class BooleanExtentions
    {
        public static string ToYesNo(this bool value)
        {
            return (value ? "Yes" : "No");
        }
    }
}
