﻿using Lobster.DecisionHelper.Common.Extensions;
using Lobster.DecisionHelper.Common.Interfaces;
using Lobster.DecisionHelper.Common.Models;
using System;
using System.Linq;

namespace Lobster.DecisionHelper.Common.DataStructures
{
    public class HierarchicalTree
    {
        private readonly int _defaultLevel = 1;
        public HierarchicalTreeNode Root { get; set; }

        public void Insert(IHierarchicalTreeObject obj)
        {
            var name = Root == null ? string.Empty : obj.IsPositive.ToYesNo();

            var newNode = new HierarchicalTreeNode(obj.Id, obj.Title, obj.CssClass, name);

            if (Root == null)
            {
                Root = newNode;

                return;
            }

            if (obj.Path.Length <= _defaultLevel)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (Root.Id == obj.ParentId.GetValueOrDefault())
            {
                Root.Childs.Add(newNode);
                return;
            }

            var level = _defaultLevel;

            var current = Root.Childs.FirstOrDefault(c => c.Id == obj.Path[_defaultLevel]);

            while (true)
            {
                level++;

                if (obj.Path.Length <= level)
                {
                    throw new ArgumentOutOfRangeException();
                }

                bool isExist = current.Childs.Any(c => c.Id == obj.Path[level]);

                if (isExist)
                {
                    current = current.Childs.FirstOrDefault(c => c.Id == obj.Path[level]);
                }
                else
                {
                    current.Childs.Add(newNode);
                    return;
                }
            }
        }
    }
}