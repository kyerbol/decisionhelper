﻿using Lobster.DecisionHelper.Common.DataStructures.Models;
using Lobster.DecisionHelper.Common.Interfaces;
using System;

namespace Lobster.DecisionHelper.Common.DataStructures
{
    public class NaiveTree : INaiveTree
    {
        private readonly int _defaultLevel = 1;
        public NaiveTreeNode Root { get; set; }

        public void Insert(INaiveTreeObject obj)
        {
            var newNode = new NaiveTreeNode(obj.Id, obj.Value, obj.ImageUrl);

            if (Root == null)
            {
                Root = newNode;

                return;
            }

            var current = Root;

            var level = _defaultLevel;


            while (true)
            {
                if(obj.Path.Length <= level)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (current.Yes?.Id == obj.Path[level])
                {
                    current = current.Yes;
                    level++;
                }
                else if (current.No?.Id == obj.Path[level])
                {
                    current = current.No;
                    level++;
                }
                else
                {
                    if (obj.IsPositive)
                    {
                        current.Yes = newNode;
                    }
                    else
                    {
                        current.No = newNode;
                    }
                    return;
                }
            }
        }
    }
}
