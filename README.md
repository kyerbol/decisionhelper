﻿# Decision Helper

Application allows you to make right decision. 

Pick a decision you have been postponing, give yourself three minutes, and just make it.

Application consists of two project:

* .Net Core API serving Questionnaires with questions.

* Angular 10 Client web for users.  


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

* Visual Studio 2019
* [.NET Core 3.1 ](https://dotnet.microsoft.com/download/dotnet-core/3.1)
* Microsoft SQL Server

### Installing & running Server

* Open solution in *Visual Studio 2019*.
* Set *.Api* project as **Startup Project** and build the project.
* If necessary, update *connectionStrings* in **appsettings.json** and specify your local SQL Database.
* Open the *Package Manager Console* from *Tools → Library Package Manager → Package Manager Console*.
* run the update-database command to apply migrations to the database.

```
	PM> update-database -project Lobster.DecisionHelper.Infrastructure
```
* Run the application.

### Installing & running Client

You need to install Node.js and then the development tools. Node.js comes with a package manager called [npm](http://npmjs.org) for installing NodeJS applications and libraries.

* [Install node.js](https://nodejs.org/en/download/)

* Install the latest version of angular-cli:

```
	npm install -g @angular/cli
```

* Install local dependencies (from the project root folder):

```
    npm install
```

(This will install the dependencies declared in the server/package.json file)

* Run the client

```
	ng serve
```

* Browse to the application at [http://localhost:4200]


## Author

* **Yerbol Kalykhbergenov** - [BitBucket](https://bitbucket.org/kyerbol)