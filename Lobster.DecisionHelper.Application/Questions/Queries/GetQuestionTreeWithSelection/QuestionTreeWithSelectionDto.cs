﻿using Lobster.DecisionHelper.Common.Models;

namespace Lobster.DecisionHelper.Application.Questions.Queries.GetQuestionTreeWithSelection
{
    /// <summary>
    /// Hierarchical question tree with selection.
    /// </summary>
    public class QuestionTreeWithSelectionDto
    {
        /// <summary>
        /// Id of the questionnaire.
        /// </summary>
        public int QuestionnaireId { get; set; }

        /// <summary>
        /// Title of questionnaire.
        /// </summary>
        public string QuestionnaireTitle { get; set; }

        /// <summary>
        /// Root of hierarchical tree.
        /// </summary>
        public HierarchicalTreeNode Root { get; set; }
    }
}