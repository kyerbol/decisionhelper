﻿using Lobster.DecisionHelper.Application.Common.Consts;
using Lobster.DecisionHelper.Application.Common.Exceptions;
using Lobster.DecisionHelper.Common.DataStructures;
using Lobster.DecisionHelper.Common.Extensions;
using Lobster.DecisionHelper.Common.Models;
using Lobster.DecisionHelper.Domain.Entities;
using Lobster.DecisionHelper.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Lobster.DecisionHelper.Application.Questions.Queries.GetQuestionTreeWithSelection
{
    public class GetQuestionTreeWithSelectionQuery : IRequest<QuestionTreeWithSelectionDto>
    {
        public int SelectedQuestionId { get; set; }

        public class GetQuestionTreeWithSelectionQueryHandler : IRequestHandler<GetQuestionTreeWithSelectionQuery, QuestionTreeWithSelectionDto>
        {
            private readonly IDecisionHelperDbContext _context;
            private readonly IConfiguration _configuration;

            public GetQuestionTreeWithSelectionQueryHandler(IDecisionHelperDbContext context, IConfiguration configuration)
            {
                _context = context;
                _configuration = configuration;
            }

            public async Task<QuestionTreeWithSelectionDto> Handle(GetQuestionTreeWithSelectionQuery request, CancellationToken cancellationToken)
            {
                var questionEntity = await _context.Questions.AsNoTracking().FirstOrDefaultAsync(q => q.Id == request.SelectedQuestionId);

                if (questionEntity == null)
                {
                    throw new NotFoundException(nameof(Question), request.SelectedQuestionId);
                }

                if (!questionEntity.Path.TrySplitByCharAndConverToIntArray(Constants.SplitChar, out int[] selectedQuestionPath))
                {
                    throw new FormatException();
                }

                var questionnaire = await _context.Questionnaires
                                            .Include(q => q.Questions)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync(q => q.Id == questionEntity.QuestionnaireId);

                var questionWithSelectionDto = new QuestionTreeWithSelectionDto { QuestionnaireId = questionnaire.Id, QuestionnaireTitle = questionnaire.Title };

                var hierarchicalTree = new HierarchicalTree();

                var questions = questionnaire.Questions.OrderBy(q => q.ParentId).ThenByDescending(q => q.IsPositive);

                foreach (var question in questions)
                {
                    if (!question.Path.TrySplitByCharAndConverToIntArray(Constants.SplitChar, out int[] path))
                    {
                        throw new FormatException();
                    }

                    var cssClass = selectedQuestionPath.Contains(question.Id) ? _configuration["AppSettings:NgxOrgChart:NgxQuestionSelectedCssClassName"] : string.Empty;

                    hierarchicalTree.Insert(new HierarchicalTreeObject(question.Id, question.Text, question.ParentId, path, cssClass, question.IsPositive));
                }

                questionWithSelectionDto.Root = hierarchicalTree.Root;

                return questionWithSelectionDto;
            }
        }
    }
}
