﻿using Lobster.DecisionHelper.Application.Common.Consts;
using Lobster.DecisionHelper.Application.Common.Exceptions;
using Lobster.DecisionHelper.Common.DataStructures;
using Lobster.DecisionHelper.Common.Extensions;
using Lobster.DecisionHelper.Common.Models;
using Lobster.DecisionHelper.Domain.Entities;
using Lobster.DecisionHelper.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaire
{
    public class GetQuestionnaireQuery : IRequest<QuestionnaireDetailDto>
    {
        public int Id { get; set; }

        public class GetQuestionnaireQueryHandler : IRequestHandler<GetQuestionnaireQuery, QuestionnaireDetailDto>
        {
            private readonly IDecisionHelperDbContext _context;

            public GetQuestionnaireQueryHandler(IDecisionHelperDbContext context)
            {
                _context = context;
            }

            public async Task<QuestionnaireDetailDto> Handle(GetQuestionnaireQuery request, CancellationToken cancellationToken)
            {
                var questionnaire = await _context.Questionnaires
                                                    .Include(q => q.Questions)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync(q => q.Id == request.Id);

                if (questionnaire == null)
                {
                    throw new NotFoundException(nameof(Questionnaire), request.Id);
                }

                var questionnaireDto = new QuestionnaireDetailDto { Title = questionnaire.Title };

                var tree = new NaiveTree();

                foreach (var question in questionnaire.Questions)
                {
                    if(!question.Path.TrySplitByCharAndConverToIntArray(Constants.SplitChar, out int[] path))
                    {
                        throw new FormatException();
                    }

                    tree.Insert(new NaiveTreeObject(question.Id, question.Text, path, question.IsPositive, question.ImageUrl));
                }

                questionnaireDto.RootQuestion = tree.Root;

                return questionnaireDto;
            }
        }
    }
}
