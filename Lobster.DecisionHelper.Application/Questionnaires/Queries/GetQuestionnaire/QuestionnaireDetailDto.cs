﻿using Lobster.DecisionHelper.Common.DataStructures.Models;

namespace Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaire
{
    /// <summary>
    /// Details of questionnaire.
    /// </summary>
    public class QuestionnaireDetailDto
    {
        /// <summary>
        /// Title of the questionnaire.
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// Root node of naive tree.
        /// </summary>
        public NaiveTreeNode RootQuestion { get; set; }
    }
}
