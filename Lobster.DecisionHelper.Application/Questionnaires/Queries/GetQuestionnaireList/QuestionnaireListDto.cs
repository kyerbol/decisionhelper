﻿using System.Collections.Generic;

namespace Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList
{
    /// <summary>
    /// Questionnaire list with list of questionnaires. 
    /// </summary>
    public class QuestionnaireListDto
    {
        /// <summary>
        /// List of questionnaires.
        /// </summary>
        public IEnumerable<QuestionnaireDto> Questionnaires { get; set; }

        public QuestionnaireListDto()
        {
            Questionnaires = new List<QuestionnaireDto>();
        }
    }
}