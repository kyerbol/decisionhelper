﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Lobster.DecisionHelper.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;

using System.Threading.Tasks;

namespace Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList
{
    public class GetQuestionnaireListQuery : IRequest<QuestionnaireListDto>
    {

        public class GetQuestionnaireListQueryHandler : IRequestHandler<GetQuestionnaireListQuery, QuestionnaireListDto>
        {
            private readonly IDecisionHelperDbContext _context;
            private readonly IMapper _mapper;

            public GetQuestionnaireListQueryHandler(IDecisionHelperDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<QuestionnaireListDto> Handle(GetQuestionnaireListQuery request, CancellationToken cancellationToken)
            {
                var questionnaires = await _context.Questionnaires
                                                .ProjectTo<QuestionnaireDto>(_mapper.ConfigurationProvider)
                                                .OrderByDescending(q => q.CreatedDateTime)
                                                .AsNoTracking()
                                                .ToListAsync();

                return new QuestionnaireListDto { Questionnaires = questionnaires };
            }
        }
    }
}
