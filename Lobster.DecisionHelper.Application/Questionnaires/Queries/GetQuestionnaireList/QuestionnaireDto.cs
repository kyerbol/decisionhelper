﻿using Lobster.DecisionHelper.Application.Common.Interfaces;
using Lobster.DecisionHelper.Domain.Entities;
using System;

namespace Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList
{
    /// <summary>
    /// Questionnaire Dto.
    /// </summary>
    public class QuestionnaireDto : IMapFrom<Questionnaire>
    {
        /// <summary>
        /// Id of the questionnaire.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of the questionnaire.
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// Created date time of the questionnaire.
        /// </summary>
        public DateTime CreatedDateTime { get; set; }
    }
}