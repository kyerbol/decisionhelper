﻿using FluentAssertions;
using Lobster.DecisionHelper.Common.DataStructures;
using Lobster.DecisionHelper.Common.Models;
using System;
using System.Linq;
using Xunit;

namespace Lobster.DecisionHelper.Common.Tests.DataStructures
{
    public class HierarchicalTreeTests
    {
        [Fact]
        public void Should_InsertNodeToRoot_When_TreeIsEmpty()
        {
            var tree = new HierarchicalTree();
            var root = new HierarchicalTreeObject(1, "Do you want to play football?", null, new int[] { 1 }, string.Empty, true);

            tree.Insert(root);

            tree.Root.Id.Should().Be(root.Id);
            tree.Root.Title.Should().Be(root.Title);
            tree.Root.Name.Should().Be(string.Empty);
        }

        [Fact]
        public void Should_ThrowArgumentOutOfRangeException_When_PathLengthLessThanChildNodes()
        {
            var tree = new HierarchicalTree();
            var root = new HierarchicalTreeObject(1, "Do you want to play football", null, new int[] { 1 }, string.Empty, true);
            tree.Insert(root);
            var question = new HierarchicalTreeObject(2, "You know how to play football?", 1, new int[] { 1 }, string.Empty, true);

            Action act = () => tree.Insert(question);

            act.Should().Throw<ArgumentOutOfRangeException>();
        }


        [Fact]
        public void Should_InsertSuccesfully_When_RootChildNodeIsAdded()
        {
            var tree = new HierarchicalTree();
            var root = new HierarchicalTreeObject(1, "Do you want to play football", null, new int[] { 1 }, string.Empty, true);
            tree.Insert(root);
            var question = new HierarchicalTreeObject(2, "You know how to play football?", 1, new int[] { 1, 2 }, string.Empty, true);

            tree.Insert(question);

            tree.Root.Childs.Count.Should().Be(1);
            tree.Root.Childs.Any(c=>c.Id == question.Id);
        }


        [Fact]
        public void Should_ThrowArgumentOutOfRangeException_When_PathLengthOf2LevelChildLessThanChildNodes()
        {
            var tree = new HierarchicalTree();
            var root = new HierarchicalTreeObject(1, "Do you want to play football", null, new int[] { 1 }, string.Empty, true);
            tree.Insert(root);
            var question = new HierarchicalTreeObject(2, "You know how to play football?", 1, new int[] { 1, 2 }, string.Empty, true);
            tree.Insert(question);
            var question2 = new HierarchicalTreeObject(3, "Is it a good weather outside??", 2, new int[] { 1, 2 }, string.Empty, true);

            Action act = () => tree.Insert(question2);

            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Should_InsertSuccesfully_When_ChildOfRootChildNodeIsAdded()
        {
            var tree = new HierarchicalTree();
            var root = new HierarchicalTreeObject(1, "Do you want to play football", null, new int[] { 1 }, string.Empty, true);
            tree.Insert(root);
            var question = new HierarchicalTreeObject(2, "You know how to play football?", 1, new int[] { 1, 2 }, string.Empty, true);
            tree.Insert(question);
            var question2 = new HierarchicalTreeObject(3, "Is it a good weather outside??", 2, new int[] { 1, 2, 3 }, string.Empty, true);

            tree.Insert(question2);

            var rootChildNode = tree.Root.Childs.FirstOrDefault(t=>t.Id == question.Id);
            rootChildNode.Should().NotBeNull();
            rootChildNode.Id.Should().Be(question.Id);
            rootChildNode.Childs.Any(c => c.Id == question2.Id);
            rootChildNode.Childs.Count.Should().Be(1);
        }
    }
}
