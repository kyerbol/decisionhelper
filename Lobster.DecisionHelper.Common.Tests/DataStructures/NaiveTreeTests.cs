﻿using FluentAssertions;
using Lobster.DecisionHelper.Common.DataStructures;
using Lobster.DecisionHelper.Common.Models;
using System;
using Xunit;

namespace Lobster.DecisionHelper.Common.Tests.DataStructures
{
    public class NaiveTreeTests
    {

        [Fact]
        public void Should_InsertNodeToRoot_When_TreeIsEmpty()
        {
            var tree = new NaiveTree();
            var root = new NaiveTreeObject(1, "Do you want to play football?", new int[] { 1 }, true, string.Empty);

            tree.Insert(root);

            tree.Root.Id.Should().Be(root.Id);
            tree.Root.Value.Should().Be(root.Value);
        }


        [Fact]
        public void Should_ThrowArgumentOutOfRangeException_When_PathLengthLessThanChildNodes()
        {
            var tree = new NaiveTree();
            var root = new NaiveTreeObject(1, "Do you want to play football?", new int[] { 1 }, true, string.Empty);
            tree.Insert(root);
            var question = new NaiveTreeObject (2, "Maybe you want to read books?", new int[] { }, false, string.Empty);

            Action act = () => tree.Insert(question);

            act.Should().Throw<ArgumentOutOfRangeException>();
        }


        [Fact]
        public void Should_InsertSuccessfully_When_PositiveChildIsAdded()
        {
            var tree = new NaiveTree();
            var root = new NaiveTreeObject(1, "Do you want to play football?", new int[] { 1 }, true, string.Empty);
            tree.Insert(root);
            var question = new NaiveTreeObject(2, "Do you know how to play football?", new int[] { 1, 2 }, true, string.Empty);

            tree.Insert(question);

            tree.Root.Yes.Id.Should().Be(question.Id);
            tree.Root.Yes.Value.Should().Be(question.Value);
        }

        [Fact]
        public void Should_InsertSuccessfully_When_NegativeChildIsAdded()
        {
            var tree = new NaiveTree();
            var root = new NaiveTreeObject(1, "Do you want to play football?", new int[] { 1 }, true, string.Empty);
            tree.Insert(root);
            var question = new NaiveTreeObject(2, "Do you know how to play football?", new int[] { 1, 2 }, false, string.Empty);

            tree.Insert(question);

            tree.Root.No.Id.Should().Be(question.Id);
            tree.Root.No.Value.Should().Be(question.Value);
        }

        [Fact]
        public void Should_InsertSuccessfully_When_3LevelPositiveiChildIsAdded()
        {
            var tree = new NaiveTree();
            var root = new NaiveTreeObject(1, "Do you want to play football?", new int[] { 1 }, true, string.Empty);
            tree.Insert(root);
            var question = new NaiveTreeObject(2, "Do you know how to play football?", new int[] { 1, 2 }, true, string.Empty);
            tree.Insert(question);
            var question2 = new NaiveTreeObject (3, "Is it hot outside?", new int[] { 1, 2, 3 }, true, string.Empty);

            tree.Insert(question2);

            tree.Root.Yes.Yes.Id.Should().Be(question2.Id);
            tree.Root.Yes.Yes.Value.Should().Be(question2.Value);
        }


        [Fact]
        public void Should_InsertSuccessfully_When_3LevelNegativeChildIsAdded()
        {
            var tree = new NaiveTree();
            var root = new NaiveTreeObject(1, "Do you want to play football?", new int[] { 1 }, true, string.Empty);
            tree.Insert(root);
            var question = new NaiveTreeObject(2, "Do you know how to play football?", new int[] { 1, 2 }, true, string.Empty);
            tree.Insert(question);
            var question2 = new NaiveTreeObject(3, "Is it hot outside?", new int[] { 1, 2, 3 }, false, string.Empty);

            tree.Insert(question2);

            tree.Root.Yes.No.Id.Should().Be(question2.Id);
            tree.Root.Yes.No.Value.Should().Be(question2.Value);
        }

    }
}
