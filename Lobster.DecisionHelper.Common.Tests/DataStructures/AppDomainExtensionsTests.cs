﻿using FluentAssertions;
using Lobster.DecisionHelper.Common.Extensions;
using System;
using Xunit;

namespace Lobster.DecisionHelper.Common.Tests.DataStructures
{
    public class AppDomainExtensionsTests
    {
        [Theory]
        [InlineData("Lobster.DecisionHelper.Apis")]
        [InlineData("Lobster.DecisionHelper.Commons")]
        [InlineData("Lobster.DecisionHelper.Applications")]
        public void Should_ReturnNull_When_NotExistingAssemblyNameIsPassed(string assemblyName)
        {
            var appDomain = AppDomain.CurrentDomain.GetAssemblyByName(assemblyName);

            appDomain.Should().BeNull();
        }

        [Theory]
        [InlineData("Lobster.DecisionHelper.Common.Tests")]
        public void Should_ReturnAssembly_When_CorrectAssemblynameIsPassed(string assemblyName)
        {
            var appDomain = AppDomain.CurrentDomain.GetAssemblyByName(assemblyName);

            appDomain.Should().NotBeNull();
            appDomain.GetName().Name.Should().Be(assemblyName);
        }

        [Theory]
        [InlineData("Lobsters.DecisionHel")]
        [InlineData("Lobster.DecisionHelpar")]
        [InlineData("Loobster.DecisionHelpe")]
        public void Should_ReturnNull_When_NotValidStartWithStringIsPassed(string startWith)
        {
            var appDomains = AppDomain.CurrentDomain.GetAssembliesStartWith(startWith);

            appDomains.Should().BeEmpty();
        }

        [Theory]
        [InlineData("Lobster.DecisionHelper")]
        public void Should_ReturnAssembly_When_ValidStartWithStringIsPassed(string startWith)
        {
            var appDomains = AppDomain.CurrentDomain.GetAssembliesStartWith(startWith);

            appDomains.Should().HaveCountGreaterThan(0);
        }

    }
}
