﻿using FluentAssertions;
using Lobster.DecisionHelper.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Lobster.DecisionHelper.Common.Tests.Extensions
{
    public class BooleanExtentionsTests
    {
        [Fact]
        public void Should_ReturnYes_When_TrueValueIsPassed()
        {
            var isSelected = true;

            var yes = isSelected.ToYesNo();

            yes.Should().Be("Yes");
        }

        [Fact]
        public void Should_ReturnNo_When_FalseValueIsPassed()
        {
            var isSelected = false;

            var no = isSelected.ToYesNo();

            no.Should().Be("No");
        }
    }
}
