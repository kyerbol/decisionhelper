﻿using FluentAssertions;
using Lobster.DecisionHelper.Common.Extensions;
using Xunit;

namespace Lobster.DecisionHelper.Common.Tests.Extensions
{
    public class StringExtensionsTests
    {
        [Fact]
        public void Should_ReturnFalse_When_NullStringIsParsed()
        {
            string input = null;
            var splitChar = '\\';

            var resultBool = input.TrySplitByCharAndConverToIntArray(splitChar, out int[] result);

            resultBool.Should().BeFalse();
            result.Should().BeEmpty();
        }


        [Theory]
        [InlineData(@"2\@4\5", '\\')]
        [InlineData(@"@3,1,56", ',')]
        [InlineData(@"10/3,51", '/')]
        public void Should_ReturnFalse_When_NotValidStringIsParsed(string input, char splitChar)
        {
            var resultBool = input.TrySplitByCharAndConverToIntArray(splitChar, out int[] result);

            resultBool.Should().BeFalse();
            result.Should().BeEmpty();
        }


        [Theory]
        [InlineData(@"2\4\5", '\\', new int[] { 2, 4, 5 })]
        [InlineData(@"3,1,56", ',', new int[] { 3, 1, 56 })]
        [InlineData(@"10/3/51", '/', new int[] { 10, 3, 51 })]
        public void Should_ReturnArrayInt_When_CorrectValueWithSplitCharIsParsed(string input, char splitChar, int[] expectedValue)
        {
            var resultBool = input.TrySplitByCharAndConverToIntArray(splitChar, out int[] result);

            result.Should().BeEquivalentTo(expectedValue);
            result.Length.Should().Be(expectedValue.Length);
            resultBool.Should().BeTrue();
        }
    }
}
