﻿using Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaire;
using Lobster.DecisionHelper.Application.Questionnaires.Queries.GetQuestionnaireList;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Lobster.DecisionHelper.Api.Controllers
{
    [Route("api/questionnaires")]
    public class QuestionnairesController : ApiController
    {
        /// <summary>
        /// Get all questionnaires.
        /// </summary>
        /// <returns>List of questionnaires.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<QuestionnaireListDto>> GetAll()
        {
            return await Mediator.Send(new GetQuestionnaireListQuery());
        }

        /// <summary>
        /// Get questionnaire detail and its questions.
        /// </summary>
        /// <param name="id">Id of the questionnaire.</param>
        /// <returns>Questionnaire detail with questions as a naive tree.</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<QuestionnaireDetailDto>> GetById(int id)
        {
            return await Mediator.Send(new GetQuestionnaireQuery { Id = id });
        }
    }
}
