﻿using Lobster.DecisionHelper.Application.Questions.Queries.GetQuestionTreeWithSelection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Lobster.DecisionHelper.Api.Controllers
{
    [Route("api/questions")]
    public class QuestionsController : ApiController
    {
        /// <summary>
        /// Get questions as a hierarchical tree with highlighted choices.
        /// </summary>
        /// <param name="id">Id of the question.</param>
        /// <returns>Questions as a hierarchical with highlighted choices.</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<QuestionTreeWithSelectionDto>> GetQuestionTreeWithSelectionById(int id)
        {
            return await Mediator.Send(new GetQuestionTreeWithSelectionQuery { SelectedQuestionId = id });
        }
    }
}
