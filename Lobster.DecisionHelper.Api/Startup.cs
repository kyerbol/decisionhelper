using Lobster.DecisionHelper.Api.Filters;
using Lobster.DecisionHelper.Application;
using Lobster.DecisionHelper.Common.Extensions;
using Lobster.DecisionHelper.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.IO;

namespace Lobster.DecisionHelper.Api
{
    public class Startup
    {
        private const string ApplicationSolutionName = "Lobster.DecisionHelper";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication();

            services.AddInfrastructure(Configuration);

            services.AddCors();

            services.AddControllers(ConfigureMvcOptions);

            services.AddSwaggerGen(ConfigureSwaggerGen);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseSwagger();

            app.UseSwaggerUI(ConfigureSwaggerUi);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


        private void ConfigureSwaggerGen(SwaggerGenOptions opt)
        {
            opt.SwaggerDoc("DecisionHelperOpenApiSpecificaton", new OpenApiInfo()
            {
                Title = "Decision Helper API",
                Version = "v1",
                Description = "Through this API you can get Questionnaires and Questions as a naive and hierarchical trees"
            });

            IncludeXmlCommentsInSwaggerGen(opt);
        }

        private void ConfigureSwaggerUi(SwaggerUIOptions opt)
        {
            opt.SwaggerEndpoint("/swagger/DecisionHelperOpenApiSpecificaton/swagger.json", "Decision Helper API");

            opt.RoutePrefix = "";
        }

        private void IncludeXmlCommentsInSwaggerGen(SwaggerGenOptions opt)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssembliesStartWith(ApplicationSolutionName);

            foreach (var assembly in assemblies)
            {
                var xmlCommentFile = $"{assembly.GetName().Name}.xml";
                var xmlCommentFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                if (File.Exists(xmlCommentFullPath))
                {
                    opt.IncludeXmlComments(xmlCommentFullPath);
                }
            }
        }

        private void ConfigureMvcOptions(MvcOptions options)
        {
            options.Filters.Add(new ApiExceptionFilterAttribute());

            options.ReturnHttpNotAcceptable = true;
        }
    }
}
