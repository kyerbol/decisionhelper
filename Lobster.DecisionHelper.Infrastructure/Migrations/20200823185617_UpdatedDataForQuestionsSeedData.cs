﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Lobster.DecisionHelper.Infrastructure.Migrations
{
    public partial class UpdatedDataForQuestionsSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3,
                column: "Text",
                value: "Right decision. It is summer outside. Go to the beach.");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3,
                column: "Text",
                value: "Right decision. It is summer outside. Go the beach.");
        }
    }
}
