﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lobster.DecisionHelper.Infrastructure.Migrations
{
    public partial class SeedQuestionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, true, null, null, "1", 1, "Are you sure you want to play football?" });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, true, null, 1, "1\\2", 1, "Do you feel yourself healthy?" });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://picsum.photos/id/100/200", false, null, 1, "1\\3", 1, "Right decision. It is summer outside. Go the beach." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://picsum.photos/id/225/200", false, null, 2, "1\\2\\4", 1, "Stay at home until you are fully recovered." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, true, null, 2, "1\\2\\5", 1, "Is it a good weather outside?" });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, null, 5, "1\\2\\5\\6", 1, "Do you have Xbox/PlayStation at home?" });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[] { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, true, null, 5, "1\\2\\5\\7", 1, "Do you like Pele?" });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreatedDateTime", "ImageUrl", "IsPositive", "LastModifiedDateTime", "ParentId", "Path", "QuestionnaireId", "Text" },
                values: new object[,]
                {
                    { 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://picsum.photos/id/96/200", true, null, 6, "1\\2\\5\\6\\10", 1, "Cool. Play FIFA at home." },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://picsum.photos/id/1058/200", true, null, 7, "1\\2\\5\\7\\8", 1, "Let’s go and show 'Ginga' style." },
                    { 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://picsum.photos/id/861/200", false, null, 7, "1\\2\\5\\7\\9", 1, "It is so sad. Do not forget to watch 'Pele' movie before playing." },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://picsum.photos/id/24/200", false, null, 7, "1\\2\\5\\6\\11", 1, "Spend your time wisely and read books." }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
