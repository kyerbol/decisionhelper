﻿using Lobster.DecisionHelper.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lobster.DecisionHelper.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DecisionHelperDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(DecisionHelperDbContext).Assembly.FullName)));

            services.AddScoped<IDecisionHelperDbContext>(provider => provider.GetService<DecisionHelperDbContext>());

            return services;
        }
    }
}
