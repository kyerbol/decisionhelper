﻿using Lobster.DecisionHelper.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Lobster.DecisionHelper.Infrastructure.Data.Seed
{
    public static class QuestionsSeedExtensions
    {
        public static void Seed(this EntityTypeBuilder<Question> builder)
        {
            builder.HasData
          (
              new Question
              {
                  Id = 1,
                  Text = "Are you sure you want to play football?",
                  IsPositive = true,
                  Path = "1",
                  QuestionnaireId = 1
              },
              new Question
              {
                  Id = 2,
                  Text = "Do you feel yourself healthy?",
                  IsPositive = true,
                  Path = @"1\2",
                  ParentId = 1,
                  QuestionnaireId = 1
              },
              new Question
              {
                  Id = 3,
                  Text = "Right decision. It is summer outside. Go to the beach.",
                  IsPositive = false,
                  Path = @"1\3",
                  ParentId = 1,
                  QuestionnaireId = 1,
                  ImageUrl = "https://picsum.photos/id/100/200"
              },
               new Question
               {
                   Id = 4,
                   Text = "Stay at home until you are fully recovered.",
                   IsPositive = false,
                   Path = @"1\2\4",
                   ParentId = 2,
                   QuestionnaireId = 1,
                   ImageUrl = "https://picsum.photos/id/225/200"
               },
                new Question
                {
                    Id = 5,
                    Text = "Is it a good weather outside?",
                    IsPositive = true,
                    Path = @"1\2\5",
                    QuestionnaireId = 1,
                    ParentId = 2
                },
                 new Question
                 {
                     Id = 6,
                     Text = "Do you have Xbox/PlayStation at home?",
                     IsPositive = false,
                     Path = @"1\2\5\6",
                     QuestionnaireId = 1,
                     ParentId = 5
                 },
                 new Question
                 {
                     Id = 7,
                     Text = "Do you like Pele?",
                     IsPositive = true,
                     Path = @"1\2\5\7",
                     QuestionnaireId = 1,
                     ParentId = 5
                 },
                 new Question
                 {
                     Id = 8,
                     Text = "Let’s go and show 'Ginga' style.",
                     IsPositive = true,
                     Path = @"1\2\5\7\8",
                     ParentId = 7,
                     QuestionnaireId = 1,
                     ImageUrl = "https://picsum.photos/id/1058/200"
                 },
                 new Question
                 {
                     Id = 9,
                     Text = "It is so sad. Do not forget to watch 'Pele' movie before playing.",
                     IsPositive = false,
                     Path = @"1\2\5\7\9",
                     ParentId = 7,
                     QuestionnaireId = 1,
                     ImageUrl = "https://picsum.photos/id/861/200"
                 },

                 new Question
                 {
                     Id = 10,
                     Text = "Cool. Play FIFA at home.",
                     IsPositive = true,
                     Path = @"1\2\5\6\10",
                     ParentId = 6,
                     QuestionnaireId = 1,
                     ImageUrl = "https://picsum.photos/id/96/200"
                 },

                 new Question
                 {
                     Id = 11,
                     Text = "Spend your time wisely and read books.",
                     IsPositive = false,
                     Path = @"1\2\5\6\11",
                     ParentId = 7,
                     QuestionnaireId = 1,
                     ImageUrl = "https://picsum.photos/id/24/200"
                 }
           );
        }
    }
}
