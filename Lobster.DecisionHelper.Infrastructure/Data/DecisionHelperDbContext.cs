﻿using Lobster.DecisionHelper.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Lobster.DecisionHelper.Infrastructure.Data
{
    public class DecisionHelperDbContext : DbContext, IDecisionHelperDbContext
    {
        public DbSet<Question> Questions { get; set; }

        public DbSet<Questionnaire> Questionnaires { get; set; }

        public DecisionHelperDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
