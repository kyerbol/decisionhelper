﻿using Lobster.DecisionHelper.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lobster.DecisionHelper.Infrastructure.Data
{
    public interface IDecisionHelperDbContext
    {
        DbSet<Questionnaire> Questionnaires { get; set; }
        DbSet<Question> Questions { get; set; }
    }
}
