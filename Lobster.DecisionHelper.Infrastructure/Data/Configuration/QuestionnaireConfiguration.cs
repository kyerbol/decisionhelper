﻿using Lobster.DecisionHelper.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Lobster.DecisionHelper.Infrastructure.Data.Configuration
{
    public class QuestionnaireConfiguration : IEntityTypeConfiguration<Questionnaire>
    {
        public void Configure(EntityTypeBuilder<Questionnaire> builder)
        {
            builder.Property(q=>q.Title)
                .IsRequired()
                .HasMaxLength(256);

            builder.HasData
                (
                    new Questionnaire
                    {
                        Id = 1,
                        Title = "Can I play football today?",
                        CreatedDateTime = new DateTime(2020, 08, 24)
                    }
                ); ;
        }
    }
}
