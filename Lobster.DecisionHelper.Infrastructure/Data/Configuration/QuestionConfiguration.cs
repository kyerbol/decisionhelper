﻿using Lobster.DecisionHelper.Domain.Entities;
using Lobster.DecisionHelper.Infrastructure.Data.Seed;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Lobster.DecisionHelper.Infrastructure.Data.Configuration
{
    public class QuestionConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.Property(q => q.Text)
             .IsRequired()
             .HasMaxLength(1024);

            builder.Property(q => q.Path)
             .IsRequired();

            builder.Property(q => q.ImageUrl)
           .HasMaxLength(128);

            builder.Property(q => q.IsPositive)
                .IsRequired();

            builder.Seed();
        }
    }
}
