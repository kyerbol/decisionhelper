﻿using Lobster.DecisionHelper.Domain.Common;

namespace Lobster.DecisionHelper.Domain.Entities
{
    public class Question : AuditableEntity
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public  bool IsPositive { get; set; }

        public int? ParentId { get; set; }

        public Question Parent { get; set; }

        public string Path { get; set; }

        public string ImageUrl { get; set; }

        public int QuestionnaireId { get; set; }

        public Questionnaire Questionnaire { get; set; }
    }
}
