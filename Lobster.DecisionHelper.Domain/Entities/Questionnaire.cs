﻿using Lobster.DecisionHelper.Domain.Common;
using System.Collections.Generic;

namespace Lobster.DecisionHelper.Domain.Entities
{
    public class Questionnaire : AuditableEntity
    {
        public int  Id { get; set; }

        public string Title { get; set; }

        public IList<Question> Questions { get; set; }

        public Questionnaire()
        {
            Questions = new List<Question>();
        }
    }
}
