﻿using System;

namespace Lobster.DecisionHelper.Domain.Common
{
    public class AuditableEntity
    {
        public DateTime CreatedDateTime { get; set; }

        public DateTime? LastModifiedDateTime { get; set; }
    }
}
